create table user(
  id SERIAL primary key unique not null auto_increment,
  login_id varchar(255) unique not null,
  name varchar(255) not null,
  birth_date DATE not null,
  password varchar(255) not null,
  create_date DATETIME not null,
  update_date DATETIME not null
);

--予め管理者として追加
insert into user(login_id, name, birth_date, password, create_date, update_date)
  values('admin','管理者', '2000-01-01', 'pass','2019-11-11 23:00:00','2019-11-11 23:00:00');
