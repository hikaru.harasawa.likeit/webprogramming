package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				HttpSession session = request.getSession(false);
				User userInfo = (User)session.getAttribute("userInfo");
				if(session == null) {
					response.sendRedirect("LoginServlet");
					return;
				}

		String loginId = request.getParameter("loginId");

		UserDao userdao = new UserDao();
		User user = userdao.findByUserId(loginId);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		if (!password.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher(
					"/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
		}else if(name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher(
					"/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
		}

		UserDao userdao = new UserDao();
		if (password == "" && password2 == "") {
			userdao.updateUser2(loginId, name, birthDate);
		}

		userdao.updateUser(loginId, name, birthDate, password);
		response.sendRedirect("UserIndexServlet");
	}
}
