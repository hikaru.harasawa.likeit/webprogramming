package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/LoginServlet")
 public class LoginServlet extends HttpServlet{
	 private static final long serialVersionUID = 1L;

	 public LoginServlet() {
		 super();
	 }

	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	 	throws ServletException, IOException{

      HttpSession session = request.getSession();
      User userInfo = (User)session.getAttribute("userInfo");
      if(userInfo != null){
        response.sendRedirect("UserIndexServlet");
        return;
      }

		 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		 dispatcher.forward(request, response);
	 }

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException{

		request.setCharacterEncoding("UTF-8");

		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");

		 UserDao userdao = new UserDao();
		 User user = userdao.findByLogin(loginId, password);

		 if(user == null) {

			 request.setAttribute("errMsg", "ログインに失敗しました");

			 RequestDispatcher dispatcher = request.getRequestDispatcher(
					 "/WEB-INF/jsp/index.jsp");
			 dispatcher.forward(request, response);
			 return;
		 }

		 //テーブルにデータが見つかった場合
		 HttpSession session = request.getSession();
		 session.setAttribute("user", user);

		 response.sendRedirect("UserIndexServlet");
	}
}
