package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserIndexServlet")
public class UserIndexServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public UserIndexServlet() {
		super();
	}


protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException{

	HttpSession session = request.getSession(false);
	User userInfo = (User)session.getAttribute("userInfo");
	if(session == null) {
		response.sendRedirect("LoginServlet");
		return;
	}

	UserDao userdao = new UserDao();
	List<User> userlist = userdao.findAll();

	request.setAttribute("userlist", userlist);

	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
	dispatcher.forward(request, response);
}

protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException{

	String login_id = request.getParameter("login_id");
	String name = request.getParameter("name");
	String startDate = request.getParameter("start_date");
	String endDate = request.getParameter("end_date");


	UserDao userdao = new UserDao();
	List<User> userlist = userdao.findByLoginInfo(login_id, name, start_date, end_date);
	request.setAttribute("userlist", userlist);

	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
	dispatcher.forward(request, response);
}
}
