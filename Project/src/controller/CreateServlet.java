package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CreateServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{

			HttpSession session = request.getSession(false);
			User userInfo = (User)session.getAttribute("userInfo");
			if(session == null) {
				response.sendRedirect("LoginServlet");
				return;
			}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException{

		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		if(!password.equals(password2)) {
			 request.setAttribute("errMsg", "入力された内容は正しくありません");
			 RequestDispatcher dispatcher = request.getRequestDispatcher(
					 "/WEB-INF/jsp/create.jsp");
			 dispatcher.forward(request, response);
			 return;
		}
		UserDao userdao = new UserDao();
		if(userdao.check(loginId) == false) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher(
					 "/WEB-INF/jsp/create.jsp");
			 dispatcher.forward(request, response);
			 return;
		}
		if(loginId.equals("") || name.equals("") || birthDate.equals("") || password.equals("") || password2.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher(
					 "/WEB-INF/jsp/create.jsp");
			 dispatcher.forward(request, response);
			 return;
		}

		userdao.CreateUser(loginId,password,name,birthDate);
		response.sendRedirect("UserIndexServlet");
	}
}
