package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public DeleteServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{

			String loginId = request.getParameter("loginId");

			UserDao userdao = new UserDao();
			userdao.deleteUser(loginId);
			response.sendRedirect("UserIndexServlet");
		}
}
