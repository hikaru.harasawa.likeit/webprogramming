package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao{

	private Connection con = null;
	private Statement stmt = null;
	private PreparedStatement ps = null;

	//暗号化
	public String encrypto(String password) throws Exception{
		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}

	//ログインIDとパスワードに紐づくユーザー情報を返す
	public User findByLoginInfo(String loginId, String password) {

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id=? and password=?";

			String p = null;
			try {
				p = encrypto(password);
			} catch (Exception e) {
				e.printStackTrace();
			}

			ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, p);
			ResultSet rs = ps.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			return new User(loginIdDate, nameDate);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//検索
	public List<User> Research(String loginId, String name, String startDate, String endDate){
		List<User> userList = new ArrayList<User>();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE name not in ('管理者')";

			if (!(loginId.equals(""))) {
				sql += " AND login_id = '" + loginId + "'";
			}
			if(!(name.equals(""))) {
				sql += " AND name LIKE'%" + name + "%'";
			}
			if(!(startDate.equals(""))) {
				sql += " AND birth_date >='" + startDate + "'";
			}
			if(!(endDate.equals(""))) {
				sql += " AND birth_date <='" + endDate + "'";
			}

			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String username = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_Date");
				String updateDate = rs.getString("update_Date");
				User user = new User(id, loginid, username, birthDate, password, createDate, updateDate);
				userList.add(user);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//全てのユーザー情報を取得
	public List<User> findAll(){
		List<User> userList = new ArrayList<User>();

		try {
			con = DBManager.getConnection();

			//管理者以外を取得
			String sql = "SELECT * FROM user WHERE name not in ('管理者')";

			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_Date");
				String updateDate = rs.getString("update_Date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				userList.add(user);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;
	}

	//新規登録
	public void CreateUser(String loginId, String password, String name, String birthDate) {
		try {
			con = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)"
					+ "	VALUES (?,?,?,?,now(),now())";

			String p = null;
				try {
						p = encrypto(password);
					} catch (Exception e) {
						e.printStackTrace();
					}

			ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, name);
			ps.setString(3, birthDate);
			ps.setString(4, p);
			ps.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(con != null) {
				try {
					con.close();

				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//userIdの重複確認
	@SuppressWarnings("null")
	public boolean check(String loginId) {
		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return (Boolean) null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return (Boolean) null;
				}
			}
		}
		return true;
	}

	//特定のユーザーの情報を取得
	public User findByUserId(String login_id) {

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id=?";

			ps = con.prepareStatement(sql);
			ps.setString(1, login_id);
			ResultSet rs = ps.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new User(id, loginId, name, birthDate, password, createDate, updateDate);

		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー情報更新
	public void updateUser(String loginId, String name, String birthDate, String password) {

		try {
			con = DBManager.getConnection();

			String sql = "UPDATE user SET name=?, birth_date=?, password=? WHERE login_id=?";

			String p = null;
			try {
				p = encrypto(password);
			} catch (Exception e) {
				e.printStackTrace();
			}

			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, birthDate);
			ps.setString(3, p);
			ps.setString(4, loginId);

			ps.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//ユーザ情報更新（name,birth_date）
	public void updateUser2(String loginId, String name, String birthDate) {

		try {
			con = DBManager.getConnection();

			String sql = "UPDATE user SET name=?, birth_date=? WHERE login_id=?";

			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, birthDate);
			ps.setString(3, loginId);
			ps.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザー情報の削除
	public void deleteUser(String loginId) {

		try {
			con = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE login_id=?";

			ps = con.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
