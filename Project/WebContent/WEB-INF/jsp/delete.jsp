<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>ユーザー削除確認</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
</head>
<body>
	<header>
		<ul class="nav justify-content-end">
      		<li class="nav-item">
       		 <a class="nav-name" tabindex="-1" aria-disabled="true">${userInfo.name}さん</a>
     		</li>
     		<li class="nav-item">
			 <a href="LogoutServlet">ログアウト</a>
			</li>
		</ul>
	</header>

	<h1>ユーザー削除確認</h1>

	<div class="confirmation">
		<p>${user.loginId}</p><br>
		<p>を本当に削除してよろしいでしょうか。</p>
	</div>

	<div class="push">
		<button type="button" onclick="location.href='UserIndexServlet'">キャンセル</button>
		<button class="ok" type="button" onclick="location.href='DeleteServlet?loginId=${user.loginId}'" style="width: 8%;">OK</button>
	</div>
</body>
</html>
