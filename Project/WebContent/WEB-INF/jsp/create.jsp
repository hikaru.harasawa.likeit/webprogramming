<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String errMsg = (String) request.getAttribute("errMsg");
%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>ユーザー新規登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="css/common.css" rel="stylesheet">
</head>
<body>
	<header>
		<ul class="nav justify-content-end">
         <li class="nav-item">
           <a class="nav-name" tabindex="-1" aria-disabled="true">${userInfo.name}さん</a>
         </li>
         <li class="nav-item">
		   <a href="LogoutServlet">ログアウト</a>
		  </li>
		 </ul>
	</header>

	<h1>ユーザー新規登録</h1>

	<% if(errMsg != null){ %>
	<p style="color: red;"><%= errMsg %></p>
	<% } %>

	<form action="CreateServlet" method="post">
	<div class="form-group row">
		<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
      <div class="col-sm-10">
        <input type="text" name="loginId">
      </div>
    </div>
    <div class="form-group row">
      <label for="password" class="col-sm-2 col-form-label">パスワード</label>
      <div class="col-sm-10">
        <input type="password" name="password">
      </div>
    </div>
    <div class="form-group row">
      <label for="password" class="col-sm-2 col-form-label">パスワード(確認)</label>
      <div class="col-sm-10">
        <input type="password" name="password2">
      </div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">ユーザ名</label>
      <div class="col-sm-10">
        <input type="text" name="name">
      </div>
    </div>
    <div class="form-group row">
      <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
      <div class="col-sm-10">
        <input type="date" name="birthDate">
      </div>
    </div>

    <div class="push">
      <button type="submit">登録</button>
    </div>
	</form>

	<div class="return">
	  <a href="UserIndexServlet">戻る</a>
	</div>
</body>
</html>
