<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>ユーザ一覧</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
</head>

<body>
<header>
	<ul class="nav justify-content-end">
      <li class="nav-item">
        <a class="nav-name" tabindex="-1" aria-disabled="true">${userInfo.name}さん</a>
      </li>
      <li class="nav-item">
		<a href="LogoutServlet">ログアウト</a>
	  </li>
	</ul>
</header>
	<h1>ユーザ一覧</h1>
	<div id="new">
		<a href="CreateServlet">新規登録</a>
	</div>

	<form action="UserIndexServlet" method="post">
	<div class="form-group row">
	<label for="loginId" class="col-md-2 col-form-label">ログインID</label>
      <div class="col-md-10">
	     <input type="text" name="loginId" style="width: 50%;">
	  </div>
	</div>
	<div class="form-group row">
      <label for="name" class="col-md-2 col-form-label">ユーザ名</label>
      <div class="col-md-10">
        <input type="text" name="name" style="width: 50%;">
      </div>
    </div>
    <div class="form-group row">
        <label for="birthDate"class="col-md-2 col-form-label">生年月日</label>
      <div class="col-md-3">
        <input type="date" name="start_date" placeholder="年/月/日">
      </div>
      <div class="col-md-1">
        ~
      </div>
      <div class="col-md-3">
        <input type="date" name="end_date" placeholder="年/月/日">
      </div>
    </div>
    <div class="form-button">
		<button type="submit" style="width: 20%;">検索</button>
	</div>
	</form>

	<table class="table table-bordered ml-auto">
    <thead>
      <tr>
        <th scope="col">ログインID</th>
        <th scope="col">ユーザ名</th>
        <th scope="col">生年月日</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
    <c:forEach var="u" items="${userlist}">
      <tr>
        <th scope="row">${u.loginId}</th>
        <td>${u.name}</td>
        <td>${u.birthDate}</td>
        <td>
        <c:choose>
        <c:when test="${userInfo.loginId == 'admin'}">
          <a class="btn btn-primary" href="DetailServlet?loginId=${u.loginId}">詳細</a>
          <a class="btn btn-success" href="UpdateServlet?loginId=${u.loginId}">更新</a>
          <a class="btn btn-danger" href="DeleteCheckServlet?loginId=${u.loginId}">削除</a>
        </c:when>
         <c:otherwise>
          <a class="btn btn-primary" href="DetailServlet?loginId=${u.loginId}">詳細</a>
              <c:if test="${userInfo.loginId == u.loginId}">
              <a class="btn btn-success" href="UpdateServlet?loginId=${u.loginId}">更新</a>
              </c:if>
         </c:otherwise>
        </c:choose>
        </td>
      </tr>
      </c:forEach>
    </tbody>
  </table>
</body>
</html>
