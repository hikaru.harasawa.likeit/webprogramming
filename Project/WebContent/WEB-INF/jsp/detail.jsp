<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>ユーザー情報詳細参照</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet">
</head>
<body>
	<header>
		<ul class="nav justify-content-end">
      		<li class="nav-item">
        		<a class="nav-name" tabindex="-1" aria-disabled="true">${userInfo.name}さん</a>
      		</li>
      		<li class="nav-item">
				<a href="LogoutServlet">ログアウト</a>
			</li>
		</ul>
	</header>

	<h1>ユーザー情報詳細参照</h1>
<form>
    <div class="form-group row">
      <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
      <div class="col-sm-10">${user.loginId}</div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">ユーザ名</label>
      <div class="col-sm-10">${user.name}</div>
    </div>
    <div class="form-group row">
      <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
      <div class="col-sm-10">${user.birthDate}</div>
    </div>
    <div class="form-group row">
      <label for="createDate" class="col-sm-2 col-form-label">登録日時</label>
      <div class="col-sm-10">${user.createDate}</div>
    </div>
    <div class="form-group row">
      <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
      <div class="col-sm-10">${user.updateDate}</div>
    </div>
  </form>

	<div class="return">
		<a href="UserIndexServlet">戻る</a>
	</div>

</body>
</html>
